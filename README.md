# RUST 实现简单cat功能

#### 介绍
RUST 实现简单cat功能。

1.5版本解决了乱码问题。

#### 软件架构
rustc 1.39.0 开发。


#### 安装教程

1.  安装rust
2.   git clone https://gitee.com/sunnyrust/sunnycat.git

#### 使用说明

1. cargo build

2. target/debug  下面有一个sunnycat

3. 执行 

   ​		./sunnycat -h

   

4. ```go
   日志检索，增强版cat.
        例子:sunnycat --keyword example --file log.txt
             ./sunnycat lines -r 5,10
        

USAGE:
    sunnycat [OPTIONS] [SUBCOMMAND]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -f, --file <FILE>          Sets the input file to use
    -k, --keyword <KEYWORD>    搜索关键字

SUBCOMMANDS:
    help     Prints this message or the help of the given subcommand(s)
    lines    选择哪些行显示




sunnycat-lines 1.5
Sunny Region. <jinheking@gmail.com>
选择哪些行显示

USAGE:
    sunnycat lines [FLAGS] [LINES]
：
FLAGS:
    -h, --help       Prints help information
    -r, --rows       输入行数，例如：-r 1,10,表示从第一行到第10行。
    -V, --version    Prints version information

ARGS:
    <LINES>     1,10,表示从第一行到第10行。

   ```

5. 

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
